Ext.define('Alamproject.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',
    requires: [
        'Ext.carousel.Carousel'
    ],

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
   /* defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },*/
    items: [{
        xtype: 'carousel',
        width:699,
        items: [{
            html: '<img src="https://cdn.pixabay.com/photo/2020/06/09/18/11/box-write-in-it-5279529_960_720.jpg">',
            layout:'fit'
        },
        {
            html: '<p>And can also use <code>ui:light</code></p>',
            cls: 'card'
        },
        {
            html: '<p>Alamproject   </p>',
            cls: 'card'
        }]
    }, {xtype: 'spacer'},    
     {
        xtype: 'carousel',
        ui: 'light',
        width:650,
        direction: 'vertical',
        items: [{
            html: '<img src="https://cdn.pixabay.com/photo/2019/04/04/15/17/smartphone-4103051_960_720.jpg">',
            cls: 'card dark'
        },
        {
            html: 'And can also use <code>ui:light</code>.',
            cls: 'card dark'
        },
        {
            html: 'Card #3',
            cls: 'card dark'
        }]
    }]
});