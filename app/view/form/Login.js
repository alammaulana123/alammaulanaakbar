Ext.define('Alamproject.view.form.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'login',
    controller: 'login',
    id: 'login',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password'    
    ],
    shadow: true,
    cls: 'demo-solid-background',
    
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetlogin',
            title: "<center>Login Your Account</center>",
            instructions: 'Please enter your account !',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'username',
                    label: 'Username',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                },
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Login',
                    ui: 'action',
                    hasDisabled: false,
                    handler: 'onLogin'
                },
                
                
            ]
        }
    ]
});