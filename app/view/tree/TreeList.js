Ext.define('Alamproject.view.tree.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],

    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',
     listeners: {
        itemtap: function(me, index, target, record, e, e0pts){
            var kat = record.data.text;
            personnelStore = Ext.getStore('personnel');
            personnelStore.filter('kat', kat);
        }
    }
});
