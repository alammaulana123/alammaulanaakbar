Ext.define('Alamproject.view.tree.TreeListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tree-list',

    formulas: {
        selectionText: function(get) {
            var selection = get('treelist.selection'),
                path;
            if (selection) {
                path = selection.getPath('id');
                path = path.replace(/^\/Root/, '');
                return 'Selected: ' + path;
            } else {
                return 'No node selected';
            }
        }
    }, 

    stores: {
        navItems: {
            type: 'tree',
            rootVisible: true,
            root: {
                expanded: true,
                text: 'Position',
                iconCls: 'x-fa fa-sitemap',
                children: [{
                    text: 'Midfielder',
                    iconCls: 'x-fa fa-users',
                    leaf: true,
                }, {
                    text: 'Attacker',
                    iconCls: 'x-fa fa-user',
                    leaf: true,
                }]
            }
        }
    }
});