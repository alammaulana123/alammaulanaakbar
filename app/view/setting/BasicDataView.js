Ext.define('Alamproject.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype: 'basicdataview',
    requires: [
        
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Alamproject.store.Personnel',
        'Ext.field.Search'
    ],
    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
            docked: 'top',
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'searchfield',
                    placeHolder: 'Nama',
                    name: 'searchfield',
                    listeners: {
                        change: function(me, newValue, oldValue, e0pts){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('name', newValue);
                        }
                    }
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'NPM',
                    name: 'searchfield',
                    listeners: {
                        change: function(me, newValue, oldValue, e0pts){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('npm', newValue);
                        }
                    }
                },
                {
                    xtype: 'searchfield',
                    placeHolder: 'Email',
                    docked: 'bottom',
                    name: 'searchfield',
                    listeners: {
                        change: function(me, newValue, oldValue, e0pts){
                            personnelStore = Ext.getStore('personnel');
                            personnelStore.filter('email', newValue);
                        }
                    }
                }
            ]
        },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<div class="img" style="fix:left"><img src="{photo}" width=200></div><div class="content"><font size=4><b>{name}</b></font><br>{kat}<br>{npm}<br>{email}<br>{phone}<hr>',
        bind: {
            store: '{personnel}',
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.img',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' +
                    '<tr><td>NPM: </td><td>{npm}</td></tr>' +
                    '<tr><td>Name: </td><td>{name}</td></tr>' +
                    '<tr><td>Position: </td><td>{kat}</td></tr>' +
                    '<tr><td>Email:</td><td>{email}</td></tr>' +
                    '<tr><td>Phone:</td><td>{phone}</td></tr>' 
                    
        }
    }]
});