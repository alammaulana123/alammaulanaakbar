/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Alamproject.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id: 'app-main',

    requires: [
        'Ext.MessageBox',
        'Alamproject.view.main.MainController',
        'Alamproject.view.main.MainModel',
        'Alamproject.view.main.List',
        'Alamproject.view.form.User',
        'Alamproject.view.group.Carousel',
        'Alamproject.view.setting.BasicDataView',
        'Alamproject.view.form.Login',
        'Alamproject.view.chart.Column',
        'Alamproject.view.chart.Radar',
        'Alamproject.view.chart.Area',
        //'Alamproject.view.tree.TreeList',
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
            
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'basicdataview'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-star',
            layout: 'fit',
            items: [{
                xtype: 'column-chart'
            }]
        },{
            title: 'Info',
            iconCls: 'x-fa fa-info-circle',
            layout: 'fit',
            items: [{
                xtype: 'chart-radar'
            }]
        },{
            title: 'Info',
            iconCls: 'x-fa fa-info-circle',
            layout: 'fit',
            items: [{
                xtype: 'chart-area'
            }]
        } ,{
            title: 'Tree',
            iconCls: 'x-fa fa-info-circle',
            layout: 'fit',
            items: [{
                xtype: 'tree-panel'
            }]
        } 
    ]
});
