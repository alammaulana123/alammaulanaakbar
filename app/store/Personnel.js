Ext.define('Alamproject.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',
    autoLoad: true,
    fields: [
    'posisi','nim', 'nama', 'email', 'nohp'
    ],

    proxy: {
        type: 'jsonp',
        api:{
              read: "http://localhost/MyApp/readPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },
});